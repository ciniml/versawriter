ImageBuilder

使い方：

1. VersaWriterをつなぐ。
2. Windowsが認識するまでまつ。
3. write.cmdに書き込みたい画像をドラッグ&ドロップする。

注意：
ブートローダはボードリビジョンに合わせた物を書き込むこと．
e.g. ボードリビジョン2 → hidboot_2.hex
