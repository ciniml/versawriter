@ECHO OFF
XCOPY /Y ..\ImageBuilder\bin\Release\* ImageBuilder\
COPY /Y ..\Bootloader\firm\VersaWriter_Revision1\hidboot.hex hidboot_1.hex
COPY /Y ..\Bootloader\firm\VersaWriter_Revision2\hidboot.hex hidboot_2.hex
COPY /Y ..\VersaWriter\Revision1\VersaWriter.hex ImageBuilder\VersaWriter_1.hex
COPY /Y ..\VersaWriter\Revision2\VersaWriter.hex ImageBuilder\VersaWriter_2.hex
PAUSE

