@ECHO OFF

REM 音を鳴らす時間(ミリ秒)
SET BEEP_DURATION=10

REM 総表示時間(ミリ秒)
SET TOTAL_DURATION=125

REM 初期遅延時間(ミリ秒)
SET INITIAL_DELAY=100

IF NOT EXIST "%~f1" (
  ECHO 画像ファイルが見つかりません - "%~f1"
  ECHO 画像ファイルをこのバッチファイルにドロップしてください．
  PAUSE
  GOTO :EOF
)

CD /D "%~dp0\ImageBuilder"

BuildImage -1 8 -2 13 --beepDuration %BEEP_DURATION% --totalDuration %TOTAL_DURATION% --initialDelay %INITIAL_DELAY% --image "%~f1"

PAUSE