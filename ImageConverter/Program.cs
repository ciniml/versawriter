﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ImageConverter
{
    class Program
    {
        const int PATTERN_HEIGHT = 10;

        static int Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.Error.WriteLine("Usage: ImageConverter <image>");
                return 1;
            }

            var path = args[0];
            var image = Image.FromFile(path);

            if (image.Height != PATTERN_HEIGHT)
            {
                Console.Error.WriteLine("Error: the image hight must equal to {0}.", PATTERN_HEIGHT);
                return 1;
            }

            // 面倒なので32bitRGBに変換する
            var bitmap = new Bitmap(image.Width, image.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            using (var g = Graphics.FromImage(bitmap))
            {
                g.DrawImageUnscaled(image, Point.Empty);
            }

            // ビットマップのピクセル配列を取得
            byte[] bits;
            int stride;
            {
                var lockBits = bitmap.LockBits(
                    new Rectangle() { X = 0, Y = 0, Width = bitmap.Width, Height = bitmap.Height },
                    System.Drawing.Imaging.ImageLockMode.ReadOnly,
                    System.Drawing.Imaging.PixelFormat.Format32bppRgb);
                bits = new byte[lockBits.Height * lockBits.Stride];
                System.Runtime.InteropServices.Marshal.Copy(lockBits.Scan0, bits, 0, bits.Length);
                stride = lockBits.Stride;
                bitmap.UnlockBits(lockBits);
            }
            Console.WriteLine("static const uint8_t _image[{0}][IMAGE_HEIGHT] = {{", image.Width);

            for (int x = 0; x < image.Width; x++)
            {
                Console.Write("\t{");
                for (int y = 0; y < image.Height / 2; y++)
                {
                    var packedValue = 0;
                    for (int i = 0; i < 2; i++)
                    {
                        var offset = x * 4 + (y * 2 + i) * stride;
                        var value = 0;
                        value |= 128 <= bits[offset + 0] ? 4 : 0;
                        value |= 128 <= bits[offset + 1] ? 2 : 0;
                        value |= 128 <= bits[offset + 2] ? 1 : 0;
                        packedValue = (packedValue << 4) | value;
                    }
                    Console.Write("0x{0:X02}, ", packedValue);
                }
                Console.WriteLine("},");
            }

            Console.WriteLine("};");
            return 0;
        }
    }
}
