/*
 * led.h
 *
 * Created: 2013/10/06 21:32:29
 *  Author: Kenta IDA
 */ 


#ifndef LED_H_
#define LED_H_

static void setColor(uint8_t color)
{
	if(color & 1) PORT_COLOR_R &= ~_BV(BIT_COLOR_R); else PORT_COLOR_R |= _BV(BIT_COLOR_R);
	if(color & 2) PORT_COLOR_G &= ~_BV(BIT_COLOR_G); else PORT_COLOR_G |= _BV(BIT_COLOR_G);
	if(color & 4) PORT_COLOR_B &= ~_BV(BIT_COLOR_B); else PORT_COLOR_B |= _BV(BIT_COLOR_B);
}
static void setSegments(uint16_t segments)
{
	if( segments & 0x001 ) PORT_SEGMENT_1  |= _BV(BIT_SEGMENT_1 ); else PORT_SEGMENT_1 &= ~_BV(BIT_SEGMENT_1 );
	if( segments & 0x002 ) PORT_SEGMENT_2  |= _BV(BIT_SEGMENT_2 ); else PORT_SEGMENT_1 &= ~_BV(BIT_SEGMENT_2 );
	if( segments & 0x004 ) PORT_SEGMENT_3  |= _BV(BIT_SEGMENT_3 ); else PORT_SEGMENT_1 &= ~_BV(BIT_SEGMENT_3 );
	if( segments & 0x008 ) PORT_SEGMENT_4  |= _BV(BIT_SEGMENT_4 ); else PORT_SEGMENT_1 &= ~_BV(BIT_SEGMENT_4 );
	if( segments & 0x010 ) PORT_SEGMENT_5  |= _BV(BIT_SEGMENT_5 ); else PORT_SEGMENT_1 &= ~_BV(BIT_SEGMENT_5 );
	if( segments & 0x020 ) PORT_SEGMENT_6  |= _BV(BIT_SEGMENT_6 ); else PORT_SEGMENT_1 &= ~_BV(BIT_SEGMENT_6 );
	if( segments & 0x040 ) PORT_SEGMENT_7  |= _BV(BIT_SEGMENT_7 ); else PORT_SEGMENT_1 &= ~_BV(BIT_SEGMENT_7 );
	if( segments & 0x080 ) PORT_SEGMENT_8  |= _BV(BIT_SEGMENT_8 ); else PORT_SEGMENT_1 &= ~_BV(BIT_SEGMENT_8 );
	if( segments & 0x100 ) PORT_SEGMENT_9  |= _BV(BIT_SEGMENT_9 ); else PORT_SEGMENT_1 &= ~_BV(BIT_SEGMENT_9 );
	if( segments & 0x200 ) PORT_SEGMENT_10 |= _BV(BIT_SEGMENT_10); else PORT_SEGMENT_1 &= ~_BV(BIT_SEGMENT_10);
}
static void selectSegment(uint8_t segment)
{
	PORT_SEGMENT_1 &= ~_BV(BIT_SEGMENT_1);
	PORT_SEGMENT_2 &= ~_BV(BIT_SEGMENT_2);
	PORT_SEGMENT_3 &= ~_BV(BIT_SEGMENT_3);
	PORT_SEGMENT_4 &= ~_BV(BIT_SEGMENT_4);
	PORT_SEGMENT_5 &= ~_BV(BIT_SEGMENT_5);
	PORT_SEGMENT_6 &= ~_BV(BIT_SEGMENT_6);
	PORT_SEGMENT_7 &= ~_BV(BIT_SEGMENT_7);
	PORT_SEGMENT_8 &= ~_BV(BIT_SEGMENT_8);
	PORT_SEGMENT_9 &= ~_BV(BIT_SEGMENT_9);
	PORT_SEGMENT_10 &= ~_BV(BIT_SEGMENT_10);
	switch(segment)
	{
		case 0: PORT_SEGMENT_1 |= _BV(BIT_SEGMENT_1); break;
		case 1: PORT_SEGMENT_2 |= _BV(BIT_SEGMENT_2); break;
		case 2: PORT_SEGMENT_3 |= _BV(BIT_SEGMENT_3); break;
		case 3: PORT_SEGMENT_4 |= _BV(BIT_SEGMENT_4); break;
		case 4: PORT_SEGMENT_5 |= _BV(BIT_SEGMENT_5); break;
		case 5: PORT_SEGMENT_6 |= _BV(BIT_SEGMENT_6); break;
		case 6: PORT_SEGMENT_7 |= _BV(BIT_SEGMENT_7); break;
		case 7: PORT_SEGMENT_8 |= _BV(BIT_SEGMENT_8); break;
		case 8: PORT_SEGMENT_9 |= _BV(BIT_SEGMENT_9); break;
		case 9: PORT_SEGMENT_10 |= _BV(BIT_SEGMENT_10); break;
	}
}

static void initLedPorts(void)
{
	DDR_COLOR_R |= _BV(BIT_COLOR_R);
	DDR_COLOR_G |= _BV(BIT_COLOR_G);
	DDR_COLOR_B |= _BV(BIT_COLOR_B);
	
	DDR_SEGMENT_1  |= _BV(BIT_SEGMENT_1);
	DDR_SEGMENT_2  |= _BV(BIT_SEGMENT_2);
	DDR_SEGMENT_3  |= _BV(BIT_SEGMENT_3);
	DDR_SEGMENT_4  |= _BV(BIT_SEGMENT_4);
	DDR_SEGMENT_5  |= _BV(BIT_SEGMENT_5);
	DDR_SEGMENT_6  |= _BV(BIT_SEGMENT_6);
	DDR_SEGMENT_7  |= _BV(BIT_SEGMENT_7);
	DDR_SEGMENT_8  |= _BV(BIT_SEGMENT_8);
	DDR_SEGMENT_9  |= _BV(BIT_SEGMENT_9);
	DDR_SEGMENT_10 |= _BV(BIT_SEGMENT_10);
}

#endif /* LED_H_ */