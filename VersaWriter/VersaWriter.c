/*
 * VersaWriter.c
 *
 * Created: 2013/07/15 1:11:02
 *  Author: Kenta IDA
 */ 


#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <stdint.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <string.h>

#define IMAGE_HEIGHT 10

#include "map.h"
#include "led.h"

static const uint8_t FLAGS_LEFT_HAND		= 0x01;
static const uint8_t FLAGS_BEEP_OVERLAPPED	= 0x02;

typedef struct tag_ImageHeader
{
	uint8_t  flags;
	uint8_t	 width;
	uint8_t	 beepPeriod[2];
	uint8_t  beepDuration;
	uint8_t  initialDelay;
	uint16_t updateInterval; 
} ImageHeader;
typedef struct tag_Image
{
	ImageHeader	header;
	uint8_t		body[][IMAGE_HEIGHT/2];
} Image;

static const __attribute__((section(".image"))) Image _image = {
	{	// header
		0,			// flags
		20,			// width
		{
			2,		// beepPeriod[0]
			4,		// beepPeriod[1]
		}, 
		10,			// beepDuration
		0,			// initialDelay
		500 / 20,	// updateInterval
	},
	{},
};

static volatile uint16_t _tick = 0;
static inline uint16_t getTick(void) { ATOMIC_BLOCK(ATOMIC_RESTORESTATE){return _tick;} }
	
static uint8_t	_buzzerCounter = 0;
static uint8_t	_buzzerPeriod = 0;
static uint8_t	_buzzerDuration = 0;
static uint16_t	_buzzerTick = 0;
static void initBuzzer(void)
{
	DDR_BUZZER_A |= _BV(BIT_BUZZER_A);
	DDR_BUZZER_B |= _BV(BIT_BUZZER_B);
	PORT_BUZZER_A &= ~_BV(BIT_BUZZER_A);
	PORT_BUZZER_B &= ~_BV(BIT_BUZZER_B);
	
	_buzzerCounter = 0;
	_buzzerPeriod = 0;
	_buzzerDuration = 0;
	_buzzerTick = 0;
}
static void beepBuzzer(uint8_t period, uint8_t duration)
{
	_buzzerCounter = 0;
	_buzzerPeriod = period;
	_buzzerDuration = duration;
	_buzzerTick = getTick();
}

static uint8_t _lineBuffer[2][IMAGE_HEIGHT];
static uint8_t _currentSegment = 0;
static uint8_t _currentBuffer = 0;
ISR(TIMER0_COMPA_vect)
{
	static uint8_t line[IMAGE_HEIGHT] = {0};
		
	setColor(0);
	selectSegment(_currentSegment);
	setColor(line[_currentSegment]);
	
	++_currentSegment;
	if( 10 <= _currentSegment)
	{
		_tick++;
		_currentSegment = 0;
		memcpy(line, &_lineBuffer[_currentBuffer & 1], IMAGE_HEIGHT);
	}
	
	// Generate buzzer beep waveform.
	if( _buzzerPeriod == 0 )
	{
		// The buzzer is not beeping.
		PORT_BUZZER_A &= ~_BV(BIT_BUZZER_A);
		PORT_BUZZER_B &= ~_BV(BIT_BUZZER_B);
	}
	else
	{
		if( _buzzerCounter < (_buzzerPeriod / 2) )
		{
			PORT_BUZZER_A |=  _BV(BIT_BUZZER_A);
			PORT_BUZZER_B &= ~_BV(BIT_BUZZER_B);
		}
		else
		{
			PORT_BUZZER_A &= ~_BV(BIT_BUZZER_A);
			PORT_BUZZER_B |=  _BV(BIT_BUZZER_B);
		}
		if( _buzzerPeriod <= ++_buzzerCounter )
		{
			_buzzerCounter = 0;
		}
		
		if( _buzzerDuration <= getTick() - _buzzerTick )
		{
			// Buzzer duration is elapsed. Stop beeping.
			_buzzerPeriod = 0;
		}
	}
}

static inline void waitTick(uint16_t period)
{
	uint16_t tick = getTick();
	while(getTick() - tick < period);
}

static void flipLineBuffer(void) 
{ 
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		_currentBuffer ^= 1; 	
	}
}	

static void setColorToSegment(uint8_t segment, uint8_t color)
{
	_lineBuffer[(_currentBuffer ^ 1) & 1][segment] = color;
}
static void clearSegments(void)
{
	memset(&_lineBuffer[(_currentBuffer ^ 1) & 1], 0, IMAGE_HEIGHT);
}

int main(void)
{
	// Restore OSCCAL
	SPMCSR |= _BV(5) | _BV(0);	// Enable SIGRD and SPMEN bit
	OSCCAL = pgm_read_byte(0x0001);
	
	initLedPorts();
	initBuzzer();
	
	TCCR0A = _BV(WGM01);	// CTC mode.
	TCCR0B = _BV(CS01);		// Prescaler = devide by 8.
	OCR0A = 100;			// CTC period = 100[us](100*8[clk])
	TIMSK0 |= _BV(OCIE0A);	// Enable Timer0 Compare match A interrupt
	
	// Load the image header from program memory.
	ImageHeader header;
	memcpy_P(&header, &_image.header, sizeof(ImageHeader));
	
	sei();
	
    while(1)
    {
		beepBuzzer(header.beepPeriod[0], header.beepDuration);
		clearSegments();
		flipLineBuffer();
		{
			uint16_t delay = header.initialDelay;
			if( !(header.flags & FLAGS_BEEP_OVERLAPPED) ) delay += header.beepDuration;
			if( delay > 0 ) waitTick(delay);
		}
		for(uint8_t x = 0; x < header.width; x++)
		{
			uint16_t tick = getTick();
			for(uint8_t i = 0; i < IMAGE_HEIGHT / 2; i++)
			{
				uint8_t packedValue = pgm_read_byte(&_image.body[header.width - x - 1][i]);
				setColorToSegment(i*2 + 0, packedValue >> 4);
				setColorToSegment(i*2 + 1, packedValue & 0x0f);
			}
			flipLineBuffer();
			while(getTick() - tick < header.updateInterval);
		}
		
		beepBuzzer(header.beepPeriod[1], header.beepDuration);
		clearSegments();
		flipLineBuffer();
		{
			uint16_t delay = header.initialDelay;
			if( !(header.flags & FLAGS_BEEP_OVERLAPPED) ) delay += header.beepDuration;
			if( delay > 0 ) waitTick(delay);
		}
		for(uint8_t x = 0; x < header.width; x++)
		{
			uint16_t tick = getTick();
			for(uint8_t i = 0; i < IMAGE_HEIGHT / 2; i++)
			{
				uint8_t packedValue = pgm_read_byte(&_image.body[x][i]);
				setColorToSegment(i*2 + 0, packedValue >> 4);
				setColorToSegment(i*2 + 1, packedValue & 0x0f);
			}
			flipLineBuffer();
			while(getTick() - tick < header.updateInterval);
		}
    }
}