﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace HidVersaWriter.Image
{
    /// <summary>
    /// イメージを表す．
    /// </summary>
    public class SparseImage : IDictionary<long, byte>
    {
        private IDictionary<long, byte> innerImage;
        private long minimumAddress = int.MaxValue;
        private long maximumAddress = int.MinValue;

        /// <summary>
        /// 最小アドレスを取得する
        /// </summary>
        public long MinimumAddress
        {
            get { return this.minimumAddress < this.maximumAddress ? this.minimumAddress : 0; }
        }

        /// <summary>
        /// 最大アドレスを取得する
        /// </summary>
        public long MaximumAddress
        {
            get { return this.minimumAddress < this.maximumAddress ? this.maximumAddress : 0; }
        }

        public SparseImage()
        {
            this.innerImage = new Dictionary<long, byte>();
        }

        public SparseImage(int capacity)
        {
            this.innerImage = new Dictionary<long, byte>(capacity);
        }

        public SparseImage(IDictionary<long, byte> based)
        {
            this.innerImage = new Dictionary<long, byte>(based);
            foreach (long address in this.innerImage.Keys)
            {
                if (address < this.minimumAddress) this.minimumAddress = address;
                if (this.maximumAddress < address) this.maximumAddress = address;
            }
        }

        /// <summary>
        /// 空間の指定された範囲をブロックに変換する．
        /// </summary>
        /// <param name="minimumAddress">範囲の下限値</param>
        /// <param name="maximumAddress">範囲の上限値(このアドレスの値も含む)</param>
        /// <returns></returns>
        public byte[] ToBlockImage(long minimumAddress, long maximumAddress)
        {
            if (maximumAddress < minimumAddress) return new byte[0];

            byte[] image = new byte[maximumAddress - minimumAddress + 1];
            for (long address = minimumAddress; address <= maximumAddress; address++)
            {
                byte value = 0;
                this.innerImage.TryGetValue(address, out value);
                image[address - minimumAddress] = value;
            }
            return image;
        }

        /// <summary>
        /// 空間全体をブロックに変換する
        /// </summary>
        /// <returns></returns>
        public byte[] ToBlockImage()
        {
            return this.ToBlockImage(this.minimumAddress, this.maximumAddress);
        }

        /// <summary>
        /// アドレスの追加に応じて，最大/最小アドレスを更新する．
        /// </summary>
        /// <param name="address">追加されたアドレス</param>
        private void CheckAddedAddress(long address)
        {
            if (address > this.maximumAddress)
                this.maximumAddress = address;
            if (address < this.minimumAddress)
                this.minimumAddress = address;
        }

        /// <summary>
        /// アドレスの削除に応じて，最大/最小アドレスを更新する．
        /// 実際にコレクションから削除する前に呼び出す必要がある．
        /// </summary>
        /// <param name="address">削除されたアドレス</param>
        private void CheckRemovedAddress(long address)
        {
            if (this.innerImage.Count == 1)
            {
                // 最後の項目が削除される．
                this.minimumAddress = int.MaxValue;
                this.maximumAddress = int.MinValue;
                return;
            }

            // 前の最大/最小アドレスから1024バイト分くらい次の最大/最小アドレスを検索する．
            if (address == this.maximumAddress)
            {
                for (int i = 0; i < 1024; i++)
                {
                    if (this.innerImage.ContainsKey(address - i))
                    {
                        this.maximumAddress = address - i;
                        break;
                    }
                }
            }
            if (address == this.minimumAddress)
            {
                for (int i = 0; i < 1024; i++)
                {
                    if (this.innerImage.ContainsKey(address + i))
                    {
                        this.minimumAddress = address + i;
                        break;
                    }
                }
            }

            if (address == this.minimumAddress || address == this.maximumAddress)
            {
                // 検索しても見つからなかった．仕方が無いので，全アドレスをソートして，次のアドレスを見つける．
                long[] addresses = new long[this.Keys.Count];
                this.Keys.CopyTo(addresses, 0);
                Array.Sort<long>(addresses);
                if (address == this.maximumAddress)
                {
                    long index = Array.Find(addresses, delegate(long target) { return target == this.maximumAddress; });
                    this.maximumAddress = addresses[index - 1];
                }
                if (address == this.minimumAddress)
                {
                    long index = Array.Find(addresses, delegate(long target) { return target == this.minimumAddress; });
                    this.minimumAddress = addresses[index + 1];
                }
            }
        }

        #region IDictionary<int,byte> メンバ

        public void Add(long key, byte value)
        {
            this.CheckAddedAddress(key);
            this.innerImage.Add(key, value);
        }

        public bool ContainsKey(long key)
        {
            return this.ContainsKey(key);
        }

        public ICollection<long> Keys
        {
            get { return this.innerImage.Keys; }
        }

        public bool Remove(long key)
        {
            this.CheckRemovedAddress(key);
            return this.innerImage.Remove(key);
        }

        public bool TryGetValue(long key, out byte value)
        {
            return this.TryGetValue(key, out value);
        }

        public ICollection<byte> Values
        {
            get { return this.innerImage.Values; }
        }

        public byte this[long key]
        {
            get { return this.innerImage[key]; }
            set
            {
                this.CheckAddedAddress(key);
                this.innerImage[key] = value;
            }
        }

        #endregion

        #region ICollection<KeyValuePair<int,byte>> メンバ

        public void Add(KeyValuePair<long, byte> item)
        {
            this.CheckAddedAddress(item.Key);
            this.innerImage.Add(item);
        }

        public void Clear()
        {
            this.innerImage.Clear();
            this.minimumAddress = int.MaxValue;
            this.maximumAddress = int.MinValue;
        }

        public bool Contains(KeyValuePair<long, byte> item)
        {
            return this.innerImage.Contains(item);
        }

        public void CopyTo(KeyValuePair<long, byte>[] array, int arrayIndex)
        {
            this.innerImage.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return this.innerImage.Count; }
        }

        public bool IsReadOnly
        {
            get { return this.innerImage.IsReadOnly; }
        }

        public bool Remove(KeyValuePair<long, byte> item)
        {
            this.CheckRemovedAddress(item.Key);
            return this.innerImage.Remove(item);
        }

        #endregion

        #region IEnumerable<KeyValuePair<int,byte>> メンバ

        public IEnumerator<KeyValuePair<long, byte>> GetEnumerator()
        {
            return this.innerImage.GetEnumerator();
        }

        #endregion

        #region IEnumerable メンバ

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return ((System.Collections.IEnumerable)this.innerImage).GetEnumerator();
        }

        #endregion
    }

    /// <summary>
    /// Hexファイルの読み込みを行う．
    /// </summary>
    public class HexLoader
    {
        private HexLoader() { } // Do not allow instantiation

        /// <summary>
        /// ストリームからイメージを読み込む．
        /// </summary>
        /// <param name="st">読み取るストリーム</param>
        /// <returns>読み込んだイメージ</returns>
        public static SparseImage Load(Stream st)
        {
            int sig = st.ReadByte();
            st.Seek(-1, SeekOrigin.Current);
            if (sig == 58)
                return LoadIntel(st);
            else
                throw new NotSupportedException(Messages.HEXLOADER_NOT_SUPPORTED);
        }

        public static SparseImage LoadIntel(Stream st)
        {
            StreamReader sr = new StreamReader(st);
            SparseImage mem = new SparseImage(1024);

            string line, data;
            int len;
            int offset;
            int type;
            int segment = 0;
            uint upperLinearAddress = 0;
            bool useLinearAddress = false;

            while (true)
            {
                line = sr.ReadLine();
                if (line[0] != ':')
                    throw new InvalidDataException(Messages.HEXLOADER_INVALID_INTEL_HEX);

                // Intel-HEXのチェックサムは、２の補数なので、チェックサムを含めて全部足すと０になる。
                if (Checksum(line.Substring(1)) != 0)
                    throw new InvalidDataException(Messages.HEXLOADER_CHECKSUM_ERROR);

                // Intel-HEX Format
                // : | (LEN) | (OFFSET) | (TYPE) | (DATA) | (CHECKSUM) |
                // 1 |   2   |    4     |    2   |    ?   |     2      |
                len = int.Parse(line.Substring(1, 2), System.Globalization.NumberStyles.HexNumber);
                offset = int.Parse(line.Substring(3, 4), System.Globalization.NumberStyles.HexNumber);
                type = int.Parse(line.Substring(7, 2), System.Globalization.NumberStyles.HexNumber);
                data = line.Substring(9, len * 2);

                switch (type)
                {
                    case 1: // End-Record
                        return mem;
                    case 0: // Data-Record
                        for (int i = 0; i < data.Length; i += 2)
                        {
                            long address;
                            if (useLinearAddress)
                                address = upperLinearAddress | (uint)((i >> 1) + offset);
                            else
                                address = (uint)((i >> 1) + offset + segment);
                            mem[address] =
                                byte.Parse(data.Substring(i, 2), System.Globalization.NumberStyles.HexNumber);
                        }
                        break;
                    case 2: // Segment Address Record
                        segment = int.Parse(data, System.Globalization.NumberStyles.HexNumber) << 4;
                        useLinearAddress = false;
                        break;
                    case 3: // Start Address Record
                        // nothing to do for this record
                        break;
                    case 4: // Extended Linear Address Record
                        upperLinearAddress = uint.Parse(data, System.Globalization.NumberStyles.HexNumber) << 16;
                        useLinearAddress = true;
                        break;
                    case 5:
                        // nothing to do for this record
                        break;
                    default:
                        throw new InvalidDataException(Messages.HEXLOADER_INVALID_RECORD);
                }
            }
        }

        private static int Checksum(string data)
        {
            string dtb;
            int sum = 0;

            for (int i = 0; i < data.Length; i += 2)
            {
                dtb = data.Substring(i, 2);
                sum += int.Parse(dtb, System.Globalization.NumberStyles.HexNumber);
                sum &= 0xff;
            }

            return sum;
        }
    }
}
