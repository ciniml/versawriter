﻿namespace HidVersaWriter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.viewSettingsFrame = new System.Windows.Forms.GroupBox();
            this.updateIntervalText = new System.Windows.Forms.TextBox();
            this.leftHandCheck = new System.Windows.Forms.CheckBox();
            this.updateIntervalLabel = new System.Windows.Forms.Label();
            this.initialDelayLabel = new System.Windows.Forms.Label();
            this.initialDelayText = new System.Windows.Forms.TextBox();
            this.settingsPanel = new System.Windows.Forms.Panel();
            this.beepSettingsFrame = new System.Windows.Forms.GroupBox();
            this.beepDurationText = new System.Windows.Forms.TextBox();
            this.beepDurationLabel = new System.Windows.Forms.Label();
            this.beepPeriodSecondText = new System.Windows.Forms.TextBox();
            this.beepPeriodSecondLabel = new System.Windows.Forms.Label();
            this.beepPeriodFirstText = new System.Windows.Forms.TextBox();
            this.beepPeriodFirstLabel = new System.Windows.Forms.Label();
            this.overlappedCheck = new System.Windows.Forms.CheckBox();
            this.mainDisplayWithScrollPanel = new System.Windows.Forms.Panel();
            this.mainDisplayPanel = new System.Windows.Forms.Panel();
            this.hScrollBar = new System.Windows.Forms.HScrollBar();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.overwriteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewSettingsFrame.SuspendLayout();
            this.settingsPanel.SuspendLayout();
            this.beepSettingsFrame.SuspendLayout();
            this.mainDisplayWithScrollPanel.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // viewSettingsFrame
            // 
            this.viewSettingsFrame.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.viewSettingsFrame.Controls.Add(this.updateIntervalText);
            this.viewSettingsFrame.Controls.Add(this.leftHandCheck);
            this.viewSettingsFrame.Controls.Add(this.updateIntervalLabel);
            this.viewSettingsFrame.Controls.Add(this.initialDelayLabel);
            this.viewSettingsFrame.Controls.Add(this.initialDelayText);
            this.viewSettingsFrame.Location = new System.Drawing.Point(5, 3);
            this.viewSettingsFrame.Name = "viewSettingsFrame";
            this.viewSettingsFrame.Size = new System.Drawing.Size(143, 113);
            this.viewSettingsFrame.TabIndex = 0;
            this.viewSettingsFrame.TabStop = false;
            this.viewSettingsFrame.Text = "表示設定";
            // 
            // updateIntervalText
            // 
            this.updateIntervalText.Location = new System.Drawing.Point(64, 62);
            this.updateIntervalText.Name = "updateIntervalText";
            this.updateIntervalText.Size = new System.Drawing.Size(65, 19);
            this.updateIntervalText.TabIndex = 11;
            // 
            // leftHandCheck
            // 
            this.leftHandCheck.AutoSize = true;
            this.leftHandCheck.Location = new System.Drawing.Point(7, 18);
            this.leftHandCheck.Name = "leftHandCheck";
            this.leftHandCheck.Size = new System.Drawing.Size(60, 16);
            this.leftHandCheck.TabIndex = 7;
            this.leftHandCheck.Text = "左手用";
            this.leftHandCheck.UseVisualStyleBackColor = true;
            // 
            // updateIntervalLabel
            // 
            this.updateIntervalLabel.AutoSize = true;
            this.updateIntervalLabel.Location = new System.Drawing.Point(3, 65);
            this.updateIntervalLabel.Name = "updateIntervalLabel";
            this.updateIntervalLabel.Size = new System.Drawing.Size(55, 12);
            this.updateIntervalLabel.TabIndex = 10;
            this.updateIntervalLabel.Text = "更新間隔:";
            // 
            // initialDelayLabel
            // 
            this.initialDelayLabel.AutoSize = true;
            this.initialDelayLabel.Location = new System.Drawing.Point(3, 40);
            this.initialDelayLabel.Name = "initialDelayLabel";
            this.initialDelayLabel.Size = new System.Drawing.Size(55, 12);
            this.initialDelayLabel.TabIndex = 8;
            this.initialDelayLabel.Text = "初期遅延:";
            // 
            // initialDelayText
            // 
            this.initialDelayText.Location = new System.Drawing.Point(64, 37);
            this.initialDelayText.Name = "initialDelayText";
            this.initialDelayText.Size = new System.Drawing.Size(65, 19);
            this.initialDelayText.TabIndex = 9;
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.beepSettingsFrame);
            this.settingsPanel.Controls.Add(this.viewSettingsFrame);
            this.settingsPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.settingsPanel.Location = new System.Drawing.Point(0, 24);
            this.settingsPanel.Name = "settingsPanel";
            this.settingsPanel.Size = new System.Drawing.Size(740, 119);
            this.settingsPanel.TabIndex = 1;
            // 
            // beepSettingsFrame
            // 
            this.beepSettingsFrame.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.beepSettingsFrame.Controls.Add(this.beepDurationText);
            this.beepSettingsFrame.Controls.Add(this.beepDurationLabel);
            this.beepSettingsFrame.Controls.Add(this.beepPeriodSecondText);
            this.beepSettingsFrame.Controls.Add(this.beepPeriodSecondLabel);
            this.beepSettingsFrame.Controls.Add(this.beepPeriodFirstText);
            this.beepSettingsFrame.Controls.Add(this.beepPeriodFirstLabel);
            this.beepSettingsFrame.Controls.Add(this.overlappedCheck);
            this.beepSettingsFrame.Location = new System.Drawing.Point(154, 3);
            this.beepSettingsFrame.Name = "beepSettingsFrame";
            this.beepSettingsFrame.Size = new System.Drawing.Size(173, 113);
            this.beepSettingsFrame.TabIndex = 1;
            this.beepSettingsFrame.TabStop = false;
            this.beepSettingsFrame.Text = "ビープ音設定";
            // 
            // beepDurationText
            // 
            this.beepDurationText.Location = new System.Drawing.Point(96, 87);
            this.beepDurationText.Name = "beepDurationText";
            this.beepDurationText.Size = new System.Drawing.Size(65, 19);
            this.beepDurationText.TabIndex = 6;
            // 
            // beepDurationLabel
            // 
            this.beepDurationLabel.AutoSize = true;
            this.beepDurationLabel.Location = new System.Drawing.Point(2, 90);
            this.beepDurationLabel.Name = "beepDurationLabel";
            this.beepDurationLabel.Size = new System.Drawing.Size(58, 12);
            this.beepDurationLabel.TabIndex = 5;
            this.beepDurationLabel.Text = "ビープ時間:";
            // 
            // beepPeriodSecondText
            // 
            this.beepPeriodSecondText.Location = new System.Drawing.Point(96, 62);
            this.beepPeriodSecondText.Name = "beepPeriodSecondText";
            this.beepPeriodSecondText.Size = new System.Drawing.Size(65, 19);
            this.beepPeriodSecondText.TabIndex = 4;
            // 
            // beepPeriodSecondLabel
            // 
            this.beepPeriodSecondLabel.AutoSize = true;
            this.beepPeriodSecondLabel.Location = new System.Drawing.Point(2, 65);
            this.beepPeriodSecondLabel.Name = "beepPeriodSecondLabel";
            this.beepPeriodSecondLabel.Size = new System.Drawing.Size(88, 12);
            this.beepPeriodSecondLabel.TabIndex = 3;
            this.beepPeriodSecondLabel.Text = "ビープ発振周期2:";
            // 
            // beepPeriodFirstText
            // 
            this.beepPeriodFirstText.Location = new System.Drawing.Point(96, 37);
            this.beepPeriodFirstText.Name = "beepPeriodFirstText";
            this.beepPeriodFirstText.Size = new System.Drawing.Size(65, 19);
            this.beepPeriodFirstText.TabIndex = 2;
            // 
            // beepPeriodFirstLabel
            // 
            this.beepPeriodFirstLabel.AutoSize = true;
            this.beepPeriodFirstLabel.Location = new System.Drawing.Point(2, 40);
            this.beepPeriodFirstLabel.Name = "beepPeriodFirstLabel";
            this.beepPeriodFirstLabel.Size = new System.Drawing.Size(88, 12);
            this.beepPeriodFirstLabel.TabIndex = 1;
            this.beepPeriodFirstLabel.Text = "ビープ発振周期1:";
            // 
            // overlappedCheck
            // 
            this.overlappedCheck.AutoSize = true;
            this.overlappedCheck.Location = new System.Drawing.Point(6, 18);
            this.overlappedCheck.Name = "overlappedCheck";
            this.overlappedCheck.Size = new System.Drawing.Size(103, 16);
            this.overlappedCheck.TabIndex = 0;
            this.overlappedCheck.Text = "オーバーラップ(&O)";
            this.overlappedCheck.UseVisualStyleBackColor = true;
            // 
            // mainDisplayWithScrollPanel
            // 
            this.mainDisplayWithScrollPanel.Controls.Add(this.mainDisplayPanel);
            this.mainDisplayWithScrollPanel.Controls.Add(this.hScrollBar);
            this.mainDisplayWithScrollPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDisplayWithScrollPanel.Location = new System.Drawing.Point(0, 143);
            this.mainDisplayWithScrollPanel.Name = "mainDisplayWithScrollPanel";
            this.mainDisplayWithScrollPanel.Size = new System.Drawing.Size(740, 311);
            this.mainDisplayWithScrollPanel.TabIndex = 2;
            // 
            // mainDisplayPanel
            // 
            this.mainDisplayPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDisplayPanel.Location = new System.Drawing.Point(0, 0);
            this.mainDisplayPanel.Name = "mainDisplayPanel";
            this.mainDisplayPanel.Size = new System.Drawing.Size(740, 288);
            this.mainDisplayPanel.TabIndex = 1;
            // 
            // hScrollBar
            // 
            this.hScrollBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.hScrollBar.Location = new System.Drawing.Point(0, 288);
            this.hScrollBar.Name = "hScrollBar";
            this.hScrollBar.Size = new System.Drawing.Size(740, 23);
            this.hScrollBar.TabIndex = 0;
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(740, 24);
            this.menuStrip.TabIndex = 3;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openMenuItem,
            this.overwriteMenuItem,
            this.saveAsMenuItem,
            this.toolStripMenuItem1,
            this.exitMenuItem});
            this.fileMenuItem.Name = "fileMenuItem";
            this.fileMenuItem.Size = new System.Drawing.Size(70, 20);
            this.fileMenuItem.Text = "ファイル(&F)";
            // 
            // openMenuItem
            // 
            this.openMenuItem.Name = "openMenuItem";
            this.openMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openMenuItem.Size = new System.Drawing.Size(195, 22);
            this.openMenuItem.Text = "開く(&O)";
            // 
            // overwriteMenuItem
            // 
            this.overwriteMenuItem.Name = "overwriteMenuItem";
            this.overwriteMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.overwriteMenuItem.Size = new System.Drawing.Size(195, 22);
            this.overwriteMenuItem.Text = "上書き保存(&S)";
            // 
            // saveAsMenuItem
            // 
            this.saveAsMenuItem.Name = "saveAsMenuItem";
            this.saveAsMenuItem.Size = new System.Drawing.Size(180, 22);
            this.saveAsMenuItem.Text = "名前を付けて保存(&A)";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(177, 6);
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitMenuItem.Text = "終了(&Q)";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 454);
            this.Controls.Add(this.mainDisplayWithScrollPanel);
            this.Controls.Add(this.settingsPanel);
            this.Controls.Add(this.menuStrip);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainForm";
            this.Text = "VersaWriter Programmer";
            this.viewSettingsFrame.ResumeLayout(false);
            this.viewSettingsFrame.PerformLayout();
            this.settingsPanel.ResumeLayout(false);
            this.beepSettingsFrame.ResumeLayout(false);
            this.beepSettingsFrame.PerformLayout();
            this.mainDisplayWithScrollPanel.ResumeLayout(false);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox viewSettingsFrame;
        private System.Windows.Forms.Panel settingsPanel;
        private System.Windows.Forms.GroupBox beepSettingsFrame;
        private System.Windows.Forms.TextBox beepPeriodSecondText;
        private System.Windows.Forms.Label beepPeriodSecondLabel;
        private System.Windows.Forms.TextBox beepPeriodFirstText;
        private System.Windows.Forms.Label beepPeriodFirstLabel;
        private System.Windows.Forms.CheckBox overlappedCheck;
        private System.Windows.Forms.TextBox beepDurationText;
        private System.Windows.Forms.Label beepDurationLabel;
        private System.Windows.Forms.TextBox updateIntervalText;
        private System.Windows.Forms.CheckBox leftHandCheck;
        private System.Windows.Forms.Label updateIntervalLabel;
        private System.Windows.Forms.Label initialDelayLabel;
        private System.Windows.Forms.TextBox initialDelayText;
        private System.Windows.Forms.Panel mainDisplayWithScrollPanel;
        private System.Windows.Forms.HScrollBar hScrollBar;
        private System.Windows.Forms.Panel mainDisplayPanel;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openMenuItem;
        private System.Windows.Forms.ToolStripMenuItem overwriteMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
    }
}