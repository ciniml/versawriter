﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HidVersaWriter
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

    }

    class Document : INotifyPropertyChanged
    {
        public string Title { get; set; }
        public bool Overwritable { get; set; }

    }
}
