﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace HidVersaWriter
{
    [Flags]
    enum ImageFlags
    {
        LeftHand = 0x01,
        BeepOverlapped = 0x02,
    }

    [DataContract(Name="ImageData", Namespace="http://fugafuga.org/VersaWriter/ImageData")]
    class ImageData
    {
        public static readonly int DefaultHeight = 10;
        
        [DataMember(IsRequired=false)]
        public ImageFlags Flags { get; set; }
        [DataMember(IsRequired=true)]
        public int Width { get; set; }
        [DataMember(IsRequired = true)]
        public int BeepPeriodFirst { get; set; }
        [DataMember(IsRequired = true)]
        public int BeepPeriodSecond { get; set; }
        [DataMember(IsRequired = true)]
        public int BeepDuration { get; set; }
        [DataMember(IsRequired = false)]
        public int InitialDelay { get; set; }
        [DataMember(IsRequired = true)]
        public int UpdateInterval { get; set; }
        [DataMember(IsRequired = true)]
        public byte[] Data { get; set; }
    }

    class ImageBuilder
    {
        public static readonly long DefaultImageDataStart = 0x4000;
        
        private static long WriteUInt8(Image.SparseImage image, long address, byte data)
        {
            image[address] = data;
            return address + 1;
        }
        private static long WriteUInt16(Image.SparseImage image, long address, UInt16 data)
        {
            address = WriteUInt8(image, address, (byte)(data & 0xff));
            address = WriteUInt8(image, address, (byte)(data >> 8));
            return address;
        }
        private static long WriteUInt32(Image.SparseImage image, long address, UInt32 data)
        {
            address = WriteUInt16(image, address, (byte)(data & 0xff));
            address = WriteUInt16(image, address, (byte)(data >> 16));
            return address;
        }
        public Image.SparseImage Build(Image.SparseImage baseImage, ImageData imageData, long dataStart)
        {
            var image = new Image.SparseImage(baseImage);
            var address = dataStart;

            address = WriteUInt8(image, address, (byte)imageData.Flags);
            address = WriteUInt8(image, address, (byte)imageData.Width);
            address = WriteUInt8(image, address, (byte)imageData.BeepPeriodFirst);
            address = WriteUInt8(image, address, (byte)imageData.BeepPeriodSecond);
            address = WriteUInt8(image, address, (byte)imageData.BeepDuration);
            address = WriteUInt8(image, address, (byte)imageData.InitialDelay);
            address = WriteUInt8(image, address, (byte)imageData.UpdateInterval);
            foreach (byte data in imageData.Data)
            {
                address = WriteUInt8(image, address, data);
            }

            return image;
        }
    }
}
