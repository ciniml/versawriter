﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Runtime.Serialization;
using Livet;

namespace VersaWriter.ViewModel
{
    class ImageDataViewModel : Livet.ViewModel
    {
        public Image.ImageData Model { get; private set; }

        public ImageDataViewModel(Image.ImageData model)
        {
            this.Model = model;
        }

        private void ValidateAndRaiseChanged<TProperty>(System.Linq.Expressions.Expression<Func<TProperty>> property)
        {
            var propertyName = ((System.Linq.Expressions.MemberExpression)property.Body).Member.Name;
            var value = this.GetType().GetProperty(propertyName).GetValue(this, new object[0]);
            var context = new ValidationContext(this, null, null) { MemberName = propertyName };
            Validator.ValidateProperty(value, context);

            this.RaisePropertyChanged(propertyName);
        }

        private bool modified;
        public bool Modified
        {
            get { return this.modified; }
            private set
            {
                this.modified = value;
                this.ValidateAndRaiseChanged(() => this.Modified);
            }
        }

        protected override void RaisePropertyChanged(string propertyName)
        {
            base.RaisePropertyChanged(propertyName);
            if (propertyName != "Modified")
            {
                this.Modified = true;
            }
        }

        public bool LeftHand
        {
            get { return (this.Model.Flags & VersaWriter.Image.ImageFlags.LeftHand) == VersaWriter.Image.ImageFlags.LeftHand; }
            set 
            {
                if (value)
                {
                    this.Model.Flags |= VersaWriter.Image.ImageFlags.LeftHand;
                }
                else
                {
                    this.Model.Flags &= ~VersaWriter.Image.ImageFlags.LeftHand;
                }
                this.ValidateAndRaiseChanged(() => this.LeftHand); 
            }
        }
        public bool BeepOverlapped
        {
            get { return (this.Model.Flags & VersaWriter.Image.ImageFlags.BeepOverlapped) == VersaWriter.Image.ImageFlags.BeepOverlapped; }
            set 
            {
                if (value)
                {
                    this.Model.Flags |= VersaWriter.Image.ImageFlags.BeepOverlapped;
                }
                else
                {
                    this.Model.Flags &= ~VersaWriter.Image.ImageFlags.BeepOverlapped;
                }
                this.ValidateAndRaiseChanged(() => this.BeepOverlapped); 
            }
        }

        [Range(0, 255)]
        public int Width
        {
            get { return this.Model.Width; }
            set { this.Model.Width = value; this.ValidateAndRaiseChanged(() => this.Width); }
        }
        [Range(0, 255)]
        public int BeepPeriodFirst
        {
            get { return this.Model.BeepPeriodFirst; }
            set { this.Model.BeepPeriodFirst = value; this.ValidateAndRaiseChanged(() => this.BeepPeriodFirst); }
        }
        [Range(0, 255)]
        public int BeepPeriodSecond
        {
            get { return this.Model.BeepPeriodSecond; }
            set { this.Model.BeepPeriodSecond = value; this.ValidateAndRaiseChanged(() => this.BeepPeriodSecond); }
        }
        [Range(0, 255)]
        public int BeepDuration
        {
            get { return this.Model.BeepDuration; }
            set { this.Model.BeepDuration = value; this.ValidateAndRaiseChanged(() => this.BeepDuration); }
        }
        [Range(0, 255)]
        public int InitialDelay
        {
            get { return this.Model.InitialDelay; }
            set { this.Model.InitialDelay = value; this.ValidateAndRaiseChanged(() => this.InitialDelay); }
        }
        [Range(0, 255)]
        public int UpdateInterval
        {
            get { return this.Model.UpdateInterval; }
            set { this.Model.UpdateInterval = value; this.ValidateAndRaiseChanged(() => this.UpdateInterval); }
        }

        private string path;
        public string Path
        {
            get { return this.path; }
            private set { this.path = value; this.RaisePropertyChanged(() => this.Path); }
        }

        public void Open()
        {
            this.Messenger.Raise(new Livet.Messaging.IO.OpeningFileSelectionMessage("Open"));
        }
        public void OpenRequested(Livet.Messaging.IO.OpeningFileSelectionMessage message)
        {
            var serializer = new DataContractSerializer(typeof(Image.ImageData));
            var filename = message.Response.SingleOrDefault();
            if (filename != null)
            {
                try
                {
                    using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        this.Model = (Image.ImageData)serializer.ReadObject(stream);
                    }
                }
                catch (IOException e)
                {
                    this.Messenger.Raise(new Livet.Messaging.InformationMessage() { Text = "ファイルを読み込むことができませんでした。", MessageKey = "OpenFailed" });
                }
                catch (InvalidDataContractException)
                {
                    this.Messenger.Raise(new Livet.Messaging.InformationMessage() { Text = "VersaWriterのイメージファイルではありません。", MessageKey = "OpenFailed" });
                }
                catch (System.Xml.XmlException)
                {
                    this.Messenger.Raise(new Livet.Messaging.InformationMessage() { Text = "VersaWriterのイメージファイルではありません。", MessageKey = "OpenFailed" });
                }

                // Notify that all properties are changed.
                foreach (var property in this.GetType().GetProperties())
                {
                    this.RaisePropertyChanged(property.Name);
                }
            }
        }
    }
}
