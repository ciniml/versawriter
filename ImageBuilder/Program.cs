﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using CommandLine;
using CommandLine.Text;

namespace VersaWriter.Util.BuildImage
{
    class Options
    {
        [Option('i', "image", Required = true, HelpText = "画像のパスを指定する。")]
        public string Image { get; set; }
        [Option('b', "baseImage", Required = false, DefaultValue = "VersaWriter.hex", HelpText = "制御用プログラムのイメージを指定する。")]
        public string BaseImage { get; set; }
        [Option('l', "leftHand", Required = false, DefaultValue = false, HelpText = "左手用のイメージを生成する。")]
        public bool LeftHand { get; set; }
        [Option('o', "beepOverlapped", Required = false, DefaultValue = false, HelpText = "ビープ音を表示の更新と並行して鳴らす。")]
        public bool BeepOverlapped { get; set; }
        [Option('d', "initialDelay", Required = false, DefaultValue = 0, HelpText = "各周期のイメージ表示開始までの遅延時間を[ms]単位で指定する。")]
        public int InitialDelay { get; set; }
        [Option('t', "totalDuration", Required = false, DefaultValue = 250, HelpText = "各周期の合計時間を[ms]単位で指定する。")]
        public int TotalDuration { get; set; }
        [Option('e', "beepDuration", Required = false, DefaultValue = 10, HelpText="ビープ音を鳴らす長さを[ms]単位で指定する。")]
        public int BeepDuration { get; set; }
        [Option('1', "beepPeriodFirst", Required = false, DefaultValue = 8, HelpText="各周期の往路の開始時にならすビープ音の発振周期を指定する。")]
        public int BeepPeriodFirst { get; set; }
        [Option('2', "beepPeriodSecond", Required = false, DefaultValue = 13, HelpText = "各周期の復路の開始時にならすビープ音の発振周期を指定する。")]
        public int BeepPeriodSecond { get; set; }
        [Option('a', "autoVersion", Required = false, DefaultValue = true, HelpText = "自動的にデバイスバージョンを認識してベースイメージを切り替える．")]
        public bool AutoVersion { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              (current) => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }

    class Program
    {
        public static int Main(string[] args)
        {
            var hidBootPath = HidBootLib.HidBoot.Enumerate().SingleOrDefault();
            if (hidBootPath == null)
            {
                Console.Error.WriteLine("Error: ブートローダが見つかりません。");
                return 1;
            }
            
            var options = new Options();
            if (Parser.Default.ParseArguments(args, options))
            {
                System.Drawing.Image image;
                try
                {
                    image = System.Drawing.Image.FromFile(options.Image);
                }
                catch (FileNotFoundException)
                {
                    Console.Error.WriteLine("Error: 画像ファイルが見つかりません。");
                    return 1;
                }
                catch (IOException)
                {
                    Console.Error.WriteLine("Error: 画像ファイルを読み込めません。");
                    return 1;
                }

                if (image.Height != 10)
                {
                    Console.Error.WriteLine("Error: 画像の高さが10pxではありません。");
                    return 1;
                }

                var imageData = new Image.ImageData()
                {
                    BeepDuration = options.BeepDuration,
                    BeepPeriodFirst = options.BeepPeriodFirst,
                    BeepPeriodSecond = options.BeepPeriodSecond,
                    InitialDelay = options.InitialDelay,
                    UpdateInterval = options.TotalDuration / image.Width,
                };
                if (options.BeepOverlapped) imageData.Flags |= Image.ImageFlags.BeepOverlapped;
                if (options.LeftHand) imageData.Flags |= Image.ImageFlags.LeftHand;

                imageData.Import(image);
                
                Image.SparseImage baseImage;
                string baseImagePath = options.BaseImage;
                using (var hid = new DeviceIO.Hid.HID(hidBootPath))
                {
                    var version = hid.Version >> 8; // メジャーバージョンのみ抽出
                    Console.WriteLine("デバイスバージョン: {0}", version);
                    if (options.AutoVersion)
                    {
                        var directory = Path.GetDirectoryName(options.BaseImage);
                        var body = Path.GetFileNameWithoutExtension(options.BaseImage);
                        var extension = Path.GetExtension(options.BaseImage);
                        baseImagePath = Path.Combine(directory, String.Format("{0}_{1}{2}", body, version, extension));
                    }
                }
                try
                {
                    using (var stream = new FileStream(baseImagePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        baseImage = Image.HexLoader.Load(stream);
                    }
                }
                catch (FileNotFoundException)
                {
                    Console.Error.WriteLine("Error: ベースイメージが見つかりませんでした - {0}", baseImagePath);
                    return 1;
                }
                var builder = new Image.ImageBuilder();
                var resultImage = builder.Build(baseImage, imageData, Image.ImageBuilder.DefaultImageDataStart);
                
                using (var hidBoot = new HidBootLib.HidBoot(hidBootPath))
                {
                    var blockImage = resultImage.ToBlockImage();
                    hidBoot.WriteApplication(blockImage, (int)resultImage.MinimumAddress, (int)resultImage.MaximumAddress);
                    hidBoot.RunApplication();
                }

                return 0;
            }
            else
            {
                return 1;
            }
        }
    }
}
