﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Drawing;

namespace VersaWriter.Image
{
    [Flags]
    public enum ImageFlags
    {
        LeftHand = 0x01,
        BeepOverlapped = 0x02,
    }

    [DataContract(Name="ImageData", Namespace="http://fugafuga.org/VersaWriter/ImageData")]
    public class ImageData
    {
        public static readonly int DefaultHeight = 10;
        
        public void Import(System.Drawing.Image image)
        {
            if (image.Height != DefaultHeight)
            {
                throw new ArgumentException(String.Format("Image height must be {0}", DefaultHeight));
            }

            // 面倒なので32bitRGBに変換する
            var bitmap = new Bitmap(image.Width, image.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            using (var g = Graphics.FromImage(bitmap))
            {
                g.DrawImageUnscaled(image, Point.Empty);
            }

            // ビットマップのピクセル配列を取得
            byte[] bits;
            int stride;
            {
                var lockBits = bitmap.LockBits(
                    new Rectangle() { X = 0, Y = 0, Width = bitmap.Width, Height = bitmap.Height },
                    System.Drawing.Imaging.ImageLockMode.ReadOnly,
                    System.Drawing.Imaging.PixelFormat.Format32bppRgb);
                bits = new byte[lockBits.Height * lockBits.Stride];
                System.Runtime.InteropServices.Marshal.Copy(lockBits.Scan0, bits, 0, bits.Length);
                stride = lockBits.Stride;
                bitmap.UnlockBits(lockBits);
            }

            var data = new byte[image.Width * DefaultHeight / 2];

            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height / 2; y++)
                {
                    var packedValue = 0;
                    for (int i = 0; i < 2; i++)
                    {
                        var offset = x * 4 + (y * 2 + i) * stride;
                        var value = 0;
                        value |= 128 <= bits[offset + 0] ? 4 : 0;
                        value |= 128 <= bits[offset + 1] ? 2 : 0;
                        value |= 128 <= bits[offset + 2] ? 1 : 0;
                        packedValue = (packedValue << 4) | value;
                    }
                    data[(image.Height * x + y * 2) / 2] = (byte)packedValue;
                }
            }

            this.Data = data;
            this.Width = image.Width;
        }
        public System.Drawing.Image Export()
        {
            var image = new Bitmap(this.Width, DefaultHeight, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
            
            var lockBits = image.LockBits(
                new Rectangle() { X = 0, Y = 0, Width = image.Width, Height = image.Height },
                System.Drawing.Imaging.ImageLockMode.WriteOnly,
                System.Drawing.Imaging.PixelFormat.Format32bppRgb);

            var bits = new byte[lockBits.Height * lockBits.Stride];
            var data = this.Data;
            for (int x = 0; x < image.Width; x++)
            {
                for (int y = 0; y < image.Height / 2; y++)
                {
                    var packedValue = data[(image.Height * x + y * 2) / 2];

                    for (int i = 0; i < 2; i++)
                    {
                        var offset = x * 4 + (y * 2 + i) * lockBits.Stride;
                        bits[offset + 0] = (byte)((packedValue & 0x40) == 0 ? 0 : 255);
                        bits[offset + 1] = (byte)((packedValue & 0x20) == 0 ? 0 : 255);
                        bits[offset + 2] = (byte)((packedValue & 0x10) == 0 ? 0 : 255);
                        packedValue = (byte)(packedValue << 4);
                    }
                }
            }

            System.Runtime.InteropServices.Marshal.Copy(bits, 0, lockBits.Scan0, bits.Length);
            image.UnlockBits(lockBits);

            return image;
        }

        [DataMember(IsRequired=false)]
        public ImageFlags Flags { get; set; }
        [DataMember(IsRequired=true)]
        public int Width { get; set; }
        [DataMember(IsRequired = true)]
        public int BeepPeriodFirst { get; set; }
        [DataMember(IsRequired = true)]
        public int BeepPeriodSecond { get; set; }
        [DataMember(IsRequired = true)]
        public int BeepDuration { get; set; }
        [DataMember(IsRequired = false)]
        public int InitialDelay { get; set; }
        [DataMember(IsRequired = true)]
        public int UpdateInterval { get; set; }
        [DataMember(IsRequired = true)]
        public byte[] Data { get; set; }
    }

    public class ImageBuilder
    {
        public static readonly long DefaultImageDataStart = 0x4000;
        
        private static long WriteUInt8(Image.SparseImage image, long address, byte data)
        {
            image[address] = data;
            return address + 1;
        }
        private static long WriteUInt16(Image.SparseImage image, long address, UInt16 data)
        {
            address = WriteUInt8(image, address, (byte)(data & 0xff));
            address = WriteUInt8(image, address, (byte)(data >> 8));
            return address;
        }
        private static long WriteUInt32(Image.SparseImage image, long address, UInt32 data)
        {
            address = WriteUInt16(image, address, (byte)(data & 0xff));
            address = WriteUInt16(image, address, (byte)(data >> 16));
            return address;
        }
        public Image.SparseImage Build(Image.SparseImage baseImage, ImageData imageData, long dataStart)
        {
            var image = new Image.SparseImage(baseImage);
            var address = dataStart;

            address = WriteUInt8(image, address, (byte)imageData.Flags);
            address = WriteUInt8(image, address, (byte)imageData.Width);
            address = WriteUInt8(image, address, (byte)imageData.BeepPeriodFirst);
            address = WriteUInt8(image, address, (byte)imageData.BeepPeriodSecond);
            address = WriteUInt8(image, address, (byte)imageData.BeepDuration);
            address = WriteUInt8(image, address, (byte)imageData.InitialDelay);
            address = WriteUInt16(image, address, (UInt16)imageData.UpdateInterval);
            foreach (byte data in imageData.Data)
            {
                address = WriteUInt8(image, address, data);
            }

            return image;
        }
    }
}
